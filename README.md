# CS 519 - Scientific Visualization

Project and Assignments for scientific visualization

## Quiz 1: mario to luigi

## Assignment 1: Color map
    - https://www.coursera.org/learn/cs-519/lecture/ZYQJs/colormaps
    - https://www.kennethmoreland.com/color-advice/
    - https://matplotlib.org/tutorials/introductory/images.html
    - https://matplotlib.org/3.3.1/tutorials/colors/colormap-manipulation.html

## Quiz 2: No real coding questions
