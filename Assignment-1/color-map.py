# Rainbow color map
import numpy as np
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap


def cm_rainbow(x):
    x = min(max(x, 0), 1)
    offset = 0.8
    c = (6-2*offset)*x + offset
    R = max(0, (3-math.fabs(c-4)-math.fabs(c-5))/2.0) # RED
    G = max(0, (4-math.fabs(c-2)-math.fabs(c-4))/2.0) # GREEN
    B = max(0, (3-math.fabs(c-1)-math.fabs(c-2))/2.0) # BLUE
    return R, G, B

mars_gray = mpimg.imread('mars.png')

lum_img = mars_gray[:, :, 0]

viridis = cm.get_cmap('viridis', 8)
print(viridis(0.56))
print('viridis.colors', viridis.colors)
print('viridis(range(8))', viridis(range(8)))
print('viridis(np.linspace(0, 1, 8))', viridis(np.linspace(0, 1, 8)))

cdict = {'red':   [[0.0,  0.0, 0.0],
                   [0.2,  0.0, 0.14],
                   [0.75,  1.0, 1.0],
                   [1.0,  0.7, 0.7]],
         'green': [[0.0,  0.0, 0.14],
                   [0.24, 0.14, 0.24],
                   [0.3, 0.7, 0.7],
                   [0.4,  0.3, 0.0],
                   [1.0,  0.0, 0.0]],
         'blue':  [[0.0,  1.0, 1.0],
                   [0.28,  0.0, 0.0],
                   [1.0,  0.0, 0.0]]}

mars_colormap = LinearSegmentedColormap('testCmap', segmentdata=cdict, N=256)
mars_red = mars_colormap(lum_img)
plt.matshow(mars_red)


if __name__ == '__main__':
    r,g,b = cm_rainbow(5)
