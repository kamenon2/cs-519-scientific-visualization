Q1.8. Turning Mario into Luigi

https://prairielearn.engr.illinois.edu/pl/course_instance/110728/instance_question/91629970/?variant_id=25080779

Turning Mario into Luigi
In this question, we will get some practice using Python and NumPy. If you need some help with Python or NumPy, you can refer to the Week 1 course reading labeled Scientific Visualization Resources. Near the bottom of that page is a section labeled Code Tutorials.


This image of Mario is given to you as the RGBA NumPy array mario. This means that it is a 2D array in which every element is a 4D NumPy array specifying an RGBA value.

As an example of how to work with this data, the following code sets the pixel at location [x,y] to be green:

luigi[x,y] = [0.0, 1.0, 0.0, 1.0]

You can use the NumPy array attribute shape to get the width and height of the image in pixels. The first two elements of that tuple will be the width and height, respectively.

You should find and replace all pixels with the color red —  — with the color green —  — and then save this as a new NumPy array named luigi. Note that Mario's hat and shirt may not be pure RGB red, so if you are finding that your colors are not changing then you may need to loosen your tolerance check.

Feel free to submit and grade your code as many times as you want. There is no limit and the best submission will count for your grade. The grading engine for this question has feedback suppressed, so you won't be able to debug by using print() or plt.imshow() and seeing output. You can download the image (right click save image as on Windows) and work with it on a local Python installation (VS Code or Spyder or Jupyter notebook) and debug there.

To do so, you can read in the image to NumPy array by using:

import matplotlib.image as mpimg

mario = mpimg.imread("mario_big.png")

Once you have gotten the code working you can just copy and paste into the browser.
You are given the following variables:

Name	Type	Description
mario	numpy array with RGBA data	Image of Mario, as shown above.

Your code snippet should define the following variables:

Name	Type	Description
luigi	numpy array with RGBA data	Green Mario, a.k.a. Luigi.
