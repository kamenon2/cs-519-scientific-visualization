Given an sRGB color  specified as a NumPy array, compute . You should set up the conversion matrix  by simply hardcoding the numerical values for the entries as seen in class.

The setup code gives the following variables:

Name	Type	Description
csrgb	numpy array (length )	A color in the sRGB space.
Your code snippet should define the following variables:

Name	Type	Description
cxyz	numpy array (length )	Product of  and .